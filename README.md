# Custom scripts

Custom scripts making my everyday work easier.

## Installation

The scripts in this repository live in a separate binary directory in my home folder (`~/bin`) and are added to my `$PATH`.

```bash
$ git clone https://gitlab.com/marcelbrueckner/bin.git ~/bin
$ echo "export PATH=$PATH:~/bin" >> ~/.bash_profile
```

## Scripts

### pdfpages

Source: https://leancrew.com/all-this/2017/04/pdf-page-counts-and-mdls/  
Alternative(s): https://apple.stackexchange.com/questions/198854/shell-command-to-count-pages-in-a-pdf-other-than-pdftk
